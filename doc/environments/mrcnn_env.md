# Installing the MaskRCNN_Watershed pipeline environment

Clone the MaskRCNN repository at  https://github.com/matterport/Mask_RCNN 
`git clone https://github.com/matterport/Mask_RCNN.git`

Navigate to the Mask_RCNN folder and save the `mrcnn_env.yaml` file inside the folder.
The `mrcnn_environment.yml` file may be found under docs/environments

Create a new Conda environment as follows:
`conda env create -f mrcnn_environment.yaml`

Within the Mask_RCNN folder, create a folder named `datasets`. Copy the MRCNN training dataset into this folder. 

Dataset provided in the data repository at: LINK

Download pre-trained COCO weights (mask_rcnn_coco.h5) from: https://github.com/matterport/Mask_RCNN/releases

Copy mask_rcnn_coco.h5 to Mask_RCNN folder .

You can now go to the training part. The watershed based post processing may be done within this environment only.
