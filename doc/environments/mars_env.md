# Installing the MARS segmentation environment

Create and activate a new environment as follows:

`conda env create --name mars --file=mars_env.yaml`
`conda activate mars`

The `mars_env.yml` file may be found under docs/environments

You can now go to the MARS segmentation pipeline. 

