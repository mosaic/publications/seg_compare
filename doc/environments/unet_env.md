# Installing the Unet_Watershed environment

Create and activate a new environment as follows:

`conda create -n unet_ws python=3.6 anaconda`
`conda activate unet_ws`

Clone the ISBI2019 branch at https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019/ 
`git clone -b ISBI2019 https://bitbucket.org/jstegmaier/cellsegmentation.git`

Install pip
`conda install -c anaconda pip`

Navigate inside the folder `cellsegmentation` and install the dependencies using:
`pip install -r requirements.txt`

The current (2020) versions of the libraries are the following:

absl-py==0.9.0
astor==0.7.1
certifi==2018.10.15
cloudpickle==0.6.1
cycler==0.10.0
dask==0.19.4
decorator==4.3.0
emoji==0.5.1
gast==0.2.2
grpcio==1.15.0
h5py==2.8.0
Keras==2.2.4
Keras-Applications==1.0.8
Keras-Preprocessing==1.0.5
kiwisolver==1.0.1
Markdown==3.0.1
matplotlib==3.0.0
networkx==2.2
numpy==1.16.0
pandas==0.23.4
Pillow==5.3.0
protobuf==3.6.1
pyparsing==2.2.2
python-dateutil==2.7.3
pytz==2018.5
PyWavelets==1.0.1
PyYAML==3.13
scikit-image==0.15
scikit-learn==0.20.0
scipy==1.1.0
setuptools==41.0.0
six==1.11.0
tensorboard==1.15.0
tensorflow-gpu==1.15
termcolor==1.1.0
toolz==0.9.0
Werkzeug==0.15

After the library installations, go to the data preparation and training step. Training command is to be run within this environment. After the training and prediction from the Unet, the final step is Watershed based post processing of Unet outputs.

The Watershed based post processing of Unet outputs have to be done in a separate environment.
To install this environment, deactivate the unet_ws environment with

`conda deactivate`

Then create the new environment with the command:

`conda env create --name mars --file=mars_env.yaml`
`conda activate mars`

The `mars_env.yaml` file may be found under docs/environments
You can then go to the final watershed based post processing part for getting 3D segmentations.
