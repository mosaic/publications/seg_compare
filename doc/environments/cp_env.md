# Installing the Cellpose training environment

Clone the Cellpose repository at  https://github.com/MouseLand/cellpose 
`git clone https://github.com/MouseLand/cellpose.git`

Navigate to the repository and create a new Conda environment as follows:
`conda env create -f cellpose_environment.yml`

The `cellpose_environment.yaml` file is under docs/environments

Activate the environment using:
`conda activate cellpose`

To install the GPU version of MKL library use : 
`pip install mxnet-cu101==version`

To check the mkl version required for your computer see here:
`https://mxnet.apache.org/versions/1.7.0/get_started?`

You can then move on to the training part.
