# Installing the Plantseg training environment

Create and activate a new environment as follows:

`conda create -n 3dunet -c conda-forge -c awolny pytorch-3dunet`
`conda activate 3dunet`

For training the Plantseg pipeline, set the command in the 3dunet environment
`train3dunet --config <PATH TO CONFIG YAML FILE>`

A sample yaml file for training is given here
