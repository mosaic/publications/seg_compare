# Installing the SegCompare environment

Create a new Conda environment as follows:
`conda env create -f segcompare_env.yaml`

Activate the environment using
`conda activate segcompare_env`

You can now go to the segmentation evaluation part.
