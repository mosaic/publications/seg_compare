## Segmentation evaluation

This repository presents tools for quantitative and in depth comparison of segmentation quality of various 3D segmentation pipelines. Segmentation outputs (.tif images) from the pipelines may be evaluated if one has the corresponding ground truth segmentated image (.tif format).
   
#### Installation

1. Download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/tree/master/envs/mars_env.yaml">  mars_env.yaml </a> file. See the folder <b>envs/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>.

2. Open a terminal and navigate to the localisation of the `mars_env.yaml` file.

3. Create and activate a new environment

    `conda env create -f mars_env.yaml`
   
#### Volume averaged Jaccard Index

This tool estimates the quality of the segmentation by calculating a weighted arithmetic mean of cell-to-cell Jaccard indices. The cell volumes are the weights. This tool estimates the segmentation errors per volume rather than per cells.

<div align="center">
<img src="_static/vaji.png" alt=general pipeline width="250" height="100"/>
<figcaption> Volume averaged Jaccard Index </figcaption>
</div>

where |Gi| is the volume of the ground truth cell (i), {Gi}  and {Pf(i)} are the sets of annotated ground truth and the corresponding predicted cells with index i.

<b>Uses</b>

1. Activate the `mars` environment:

    `conda activate mars`
    
2. Download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/notebooks/Volume averaged Jaccard index evaluation.ipynb">  Volume averaged Jaccard index evaluation </a> notebook (folder <b>notebooks/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>).

3. Open the Jupyter file browser using:
 
    `jupyter notebook`

4. Navigate in the browser to the jupyter notebook. Open it.

5. In the notebook, update the cell containing the image data localisation.

6. Run the notebook.

#### Segmentation qualities in terms of rates of correct segmentations, over/undersegmentations

This tool estimates the type of the errors encountered in a predicted segmentation. It is based on the segmentation comparison method developed by G.Michelin [1].

[1] Michelin, G. (2016, October). Outils d’analyse d’images et recalage d’individus pour l’étude de la morphogenèse animale et végétale (Doctoral dissertation)

<div align="center">
<img src="_static/segqual.png" alt=general pipeline width="700" height="250"/>
<figcaption> Segmentation quality: Over (red)/ Under (blue)/Correct (green) segmentation </figcaption>
</div>

<b>Uses</b>

1. Activate the `mars` environment:

    `conda activate mars`
    
2. Download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/notebooks/Characterization of the segmentation errors (underseg, overseg,...).ipynb">  Characterization of the segmentation errors (underseg, overseg,...) </a> notebook (folder <b>notebooks/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>).

3. Open the Jupyter file browser using:
 
    `jupyter notebook`

4. Navigate in the browser to the jupyter notebook. Open it.

5. In the notebook, update the cells containing the image data localisation.

6. Run the notebook.
