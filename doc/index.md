```{include} ..
:relative-docs: ../
:relative-images:
```

```{include} concept.md
```

## Table of contents

* [Datasets](descriptions/dataset)
* [Installation instructions](install)
* [Segmentation pipelines ](pipeline)
* [Evaluation](evaluation)
* [3D Visualization](descriptions/morphonet)
* [Downloads](citation)


