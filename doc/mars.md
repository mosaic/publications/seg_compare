### MARS
  * Original repository: https://mosaic.gitlabpages.inria.fr/timagetk/
  * Concept:  MARS is a watershed based pipeline and uses automatic seed detection. It uses the minima of local intensity of the image (or the hminima parameter) to detect seed regions which are used for 3D watershed segmentation.
  * ![Example screenshot](_static/mars_pipeline.png)

## Installation instructions

  * Type: Jupyter notebook application
  * Pre-processing steps: None
  * Data preparation: None
  * Training environment: Invoke the segmentation environment using :
        `conda activate MARS`. 
  * Model training: None
  * Sample results: DATA_REPO_LINK
