# Installation instructions

In this section you will find instructions to install the environments for running the different segmentation pipelines. The pipelines often use different Python and library versions and therefore in order to avoid conflict during execution, separate environments should be used. The environments can be setup using the yaml files whose links are provided. The yaml files may be used for:

* Installing segmentation pipeline environments
* Installing evaluation and visualization environment

## Pre-requisite

All the segmentation pipelines and evaluation/visualization methods are implemented using Python 3.x . So first you will need to install an <a href="https://docs.anaconda.com/anaconda/install/">  anaconda distribution </a>  suitable for your OS.

## Segmentation pipeline: PlantSeg

Create and activate a new environment as follows:

`conda create -n 3dunet -c conda-forge -c awolny pytorch-3dunet`

`conda activate 3dunet`

For training the Plantseg pipeline, set the command in the 3dunet environment:

`train3dunet --config <PATH TO CONFIG YAML FILE>`

A sample yaml file for training is given here: <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/plantseg_train.yaml">  https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/plantseg_train.yaml </a> 

## Segmentation pipeline: MARS (3D Watershed)

Create and activate a new environment as follows:

`conda env create --name mars --file=mars_env.yaml`

`conda activate mars`

The `mars_env.yml` file may be found here:<a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/mars_env.yaml">  https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/mars_env.yaml </a> 

You can now go to the MARS segmentation pipeline. 

## Segmentation pipeline: 3D UNet with Watershed

Create and activate a new environment as follows:

`conda create -n unet_ws python=3.6 anaconda`

`conda activate unet_ws`

Clone the ISBI2019 branch at https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019/ 

`git clone -b ISBI2019 https://bitbucket.org/jstegmaier/cellsegmentation.git`

Install pip
`conda install -c anaconda pip`

Navigate inside the folder `cellsegmentation` and install the dependencies using:

`pip install -r requirements.txt`

The current versions of the libraries is given here:<a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/requirements.md">  https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/requirements.md </a> 

After the library installations, go to the data preparation and training step. Training command is to be run within this environment. After the training and prediction from the Unet, the final step is Watershed based post processing of Unet outputs.

For post processing of Unet outputs the MARS environment maybe used. Instructions to install it are given above.

To run MARS 3D watershed first deactivate the unet_ws environment with

`conda deactivate`

Then activate MARS environment with:

`conda activate mars`

You can then go to the final watershed based post processing part for getting 3D segmentations.

##  Segmentation pipeline: Cellpose
Clone the Cellpose repository at  https://github.com/MouseLand/cellpose 

`git clone https://github.com/MouseLand/cellpose.git`

Navigate to the repository and create a new Conda environment as follows:

`conda env create -f cellpose_environment.yml`

The `cellpose_environment.yaml` file may be found here: <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/cellpose_environment.yaml">  https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/cellpose_environment.yaml </a> 

Activate the environment using:

`conda activate cellpose`

To install the GPU version of MKL library use : 

`pip install mxnet-cu101==version`

To check the mkl version required for your computer see here:

`https://mxnet.apache.org/versions/1.7.0/get_started?`

You can then move on to the training part.

##  Segmentation pipeline: Mask RCNN with Watershed

Clone the MaskRCNN repository at  https://github.com/matterport/Mask_RCNN 

`git clone https://github.com/matterport/Mask_RCNN.git`

Navigate to the Mask_RCNN folder and save the `mrcnn_env.yaml` file inside the folder.

The `mrcnn_env.yml` file may be found here:<a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/mrcnn_env.yaml">  https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/environments/mrcnn_env.yaml </a> 

Create a new Conda environment as follows:

`conda env create -f mrcnn_env.yaml`

Within the Mask_RCNN folder, create a folder named `datasets`. 

Copy the MRCNN training dataset into this folder. For link to a sample training dataset go to Datasets section.

Download pre-trained COCO weights (mask_rcnn_coco.h5) from: https://github.com/matterport/Mask_RCNN/releases

Copy mask_rcnn_coco.h5 to Mask_RCNN folder .

You can now go to the training part. The watershed based post processing may be done within this environment only.
