# Plantseg
  * <b>Original repository</b>:   <a href="https://github.com/hci-unihd/plant-seg">  https://github.com/hci-unihd/plant-seg </a> 
  * <b>License </b> : MIT License 
  * <b>Paper citation </b> : <i>Wolny A, Cerrone L, Vijayan A, Tofanelli R, Barro AV, Louveaux M, et al. Accurate and versatile 3D segmentation of plant tissues at cellular resolution. elife. 2020;9. doi:10.7554/eLife.57613 </i>
  * <b>Concept</b>: A residual 3D-UNet  is used for prediction of cell boundaries from a 3D image followed by using graph partitioning techniques such as GASP, Mutex and Multicut on predicted cell boundaries to get instance level segmentation.

   <div align="center">
<img src="../_static/ps_pipeline.png" alt="Plantseg pipeline" width="700" height="200"/>
 <figcaption>Schematic diagram of the Plantseg pipeline</figcaption>
</div>

#### Installation

1. Open a terminal and create a new environment as follows:

    `conda create -n 3dunet -c conda-forge -c awolny pytorch-3dunet`

#### Uses

The segmentation of an image using PlantSeg method requires a trained model. This model can be obtained in two different ways:

* Download one of the proposed <a href="https://doi.org/10.6084/m9.figshare.14433590.v1">  trained models </a>
* Train your own model using either your data or the proposed <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> training data  </a> (see [Datasets](dataset.md)).

  * <b>Type</b>: GUI application. To run the Plantseg pipeline, browse for the image, select the trained model (under Model Name), pre-processing and post processing parameters and click Run. 

<div align="center">
<img src="../_static/ps_sig.png" alt="Plantseg pipeline" width="1000" height="500"/>
<figcaption>Graphical user interface for running the Plantseg pipeline</figcaption>
</div>


  To install and start the GUI application run the commands:

  ```
  conda create -n plant-seg -c lcerrone -c abailoni -c cpape -c awolny -c conda-forge nifty=vplantseg1.0.8 pytorch-3dunet=1.2.5 plantseg

  conda activate plant-seg

  plantseg --gui
  ```

  * <b>Pre-processing steps</b>: Rescaling images to XYZ resolution of training data, image interpolation, Gaussian or median filtering. Functions present within GUI
  * <b>Dataset preparation</b>:  The training data comprises of .h5 files each of which contains 1 image and corresponding ground truth image. 
  * <b>Training environment</b>: Follow instructions under "Installation" for installing the Plantseg training environment.
  * <b>Model training</b>: The training configurations are specified in the yaml file named "plantseg_train_config" as provided under docs/environments. The training may be invoked using the following command: 

      `train3dunet --config <PATH TO CONFIG YAML FILE>`. 

 The trained model (best_checkpoint.pytorch, last_checkpoint.pytorch) and the configuration file used for training may be uploaded to the Plantseg GUI using the "Add Custom Model" function in the GUI.

  * <b>Post-processing</b>:  Use either GASP, Mutex or Multicut functions on the GUI to get final segmentation. 
  * <b>Training data</b>: <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> https://www.repository.cam.ac.uk/handle/1810/262530  </a> 
  * <b>Trained models</b>: <a href="https://doi.org/10.6084/m9.figshare.14433590.v1">  Link to trained model </a> 

#### Results

You can find sample segmentation results and ground truth at this <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  link </a>. 
