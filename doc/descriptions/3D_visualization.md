# 3D visualization of segmentation quality

 * Concept: For visualizing quality of outputs from different  3D segmentation pipelines, the browser based visualization platform named Morphonet (https://morphonet.org) is used. 

First, the Jaccard index values are computed for a segmented stack with respect to its ground truth. 
Then the 3D segmented stack (in .tif format) is converted to a 3D mesh using Marching Cubes algorithm and the mesh is uploaded to the  Morphonet platform along with the  Jaccard index values. Jaccard indices for each cell can then be viewed as colormaps in a 3D interactive visualization on Morphonet. 


Benefits: Users can a) check the Jaccard Index value for each cell by clicking on it b) impose a color mapping of the Jaccard index values  c) locate cell regions where poor Jaccard values are concentrated d) interact with the segmented dataset in 3D, e.g rotate 360 degrees/zoom/slice through the segmented data in XYZ directions e) inspect segmentation quality in any inner cell layers 

<div align="center">
<img src="../_static/mn_disp.png" alt="Plantseg pipeline" width="700" height="300"/>

</div>

 
 * The steps for this 3D visualization are below. You will need to:
    * Have a segmented stack and the corresponding ground truth stack
    * Create and/or start the mars environment using the following :

      `conda env create -f mars_env.yaml`

      `conda activate mars`

    * Use the two stacks and run the Volume averaged Volume averaged Jaccard index evaluation.ipynb in this environment. Save the outputs of the ipynb by running the last cell (saves the JI results in a .csv file)
    * Use the 3D_visualization.ipynb to create and upload the information to Morphonet. 
    * Open https://morphonet.org and login with your username and password. You can now view the dataset with the uploaded mesh. Uner the infos tab you can view the Jaccard Index information you uploaded. To apply the and Jaccard index values as colormap on the mesh, follow the steps in https://morphonet.org/help_app.

  
