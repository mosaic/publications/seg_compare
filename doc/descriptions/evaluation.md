## Segmentation evaluation

  * Concept: This repository presents tools for quantitative and in depth comparison of segmentation quality of various 3D segmentation pipelines. Segmentation outputs (.tif images) from the pipelines may be evaluated if one has the corresponding ground truth segmentated image (.tif format). The evaluation metrics are :

    * Volume averaged Jaccard Index

        <div align="center">
        <img src="../_static/vaji.png" alt=general pipeline width="500" height="450"/>
        <figcaption> Volume averaged Jaccard Index </figcaption>
        </div>
        
        where |Gi| is the volume of the ground truth cell (i), {Gi}  and {Pf(i)} are the sets of annotated ground truth and the corresponding predicted cells with index i.

    * Segmentation qualities in terms of rates of correct segmentations, over/undersegmentations

        <div align="center">
        <img src="../_static/segqual.png" alt=general pipeline width="600" height="250"/>
        <figcaption> Dice score </figcaption>
        </div>


 * For using these tools first create and activate the segmentation comparison environment 
 
             `conda env create -f mars_env.yaml`
             `conda activate mars`

    For the evaluation tasks, follow the steps below:
      * For Jaccard index estimation use the notebook: <b> Volume averaged Jaccard index evaluation.ipynb</b>
      * For Dice score estimation use the notebook: <b>Dice_Score.ipynb</b>
      * For rates of under/over segmentation: <b>Characterization of the segmentation errors (underseg, overseg,...).ipynb</b>
      * Input to all the evaluation tools are 1) 3D segmented image  2) 3D ground truth image 
