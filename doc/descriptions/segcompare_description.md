# Segmentation comparison

  * Concept: This repository presents tools for quantitative and in depth comparison of segmentation quality of various 3D segmentation pipelines. Segmentation outputs (.tif images) from the pipelines may be evaluated if one has the corresponding ground truth segmentated image (.tif format). The evaluation metrics are :

    * Volume averaged Jaccard Index
    * Dice score
    * Segmentation qualities in terms of rates of correct segmentations, over/undersegmentations, confusion with background 
    * Layerwise averaged Jaccard Index

 * For using these tools first create and activate the segmentation comparison environment 
 
             `conda env create -f segcompare_env.yaml`
             `conda activate segcompare_env`
 
    For the evaluation tasks, follow the steps below:
      * For Jaccard index estimation use the notebook: Jaccard_Index.ipynb
      * For layerwise averaged Jaccard index: Layerwise_evaluation.ipynb
      * For Dice score estimation use the notebook: Dice_Score.ipynb
      * For rates of under/over segmentation: Segmentation_Quality.ipynb
      * Input to all the evaluation tools are 1) 3D segmented image (in .tif format) 2) 3D ground truth image (in .tif format) 
