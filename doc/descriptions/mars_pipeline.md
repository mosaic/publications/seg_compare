## MARS
  *  <b> Original repository </b>:  <a href=" https://mosaic.gitlabpages.inria.fr/timagetk">  https://mosaic.gitlabpages.inria.fr/timagetk </a> 
  *  <b> License </b> : INRIA license 
  *  <b> Paper citation </b> : <i>Fernandez R, Das P, Mirabet V, Moscardi E, Traas J, Verdeil J-L, et al. Imaging plant growth in 4D: robust tissue reconstruction and lineaging at cell resolution. Nat Methods. 2010;7: 547–553. doi:10.1038/nmeth.1472 </i>

  * <b>Concept</b>: A highly efficient 3D watershed based segmentation pipeline which uses automatic seed detection. Seeds  locations are estimated from the minima of local intensity of the image (or the hminima parameter) which are then used for 3D watershed segmentation.

<div align="center">
<img src="../_static/mars_pipeline.png" alt="MARS pipeline" width="700" height="200"/>
 <figcaption>Schematic diagram of the MARS pipeline</figcaption>
</div>

#### Installation

1. Download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/tree/master/envs/mars_env.yaml">  mars_env.yaml </a> file. See the folder <b>envs/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>.

2. Open a terminal and navigate to the localisation of the `mars_env.yaml` file.

3. Create and activate a new environment

    `conda env create --name mars --file=mars_env.yaml`
    
    `conda activate mars`

#### Uses
  
1. Activate the conda environment:
 
    `conda activate mars`

2. Download the jupyter notebook <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/notebooks/MARS_segmentation.ipynb">  MARS_segmentation.ipynb </a>. See the folder <b>notebooks/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>.
  
3. Open the Jupyter file browser using:
 
    `jupyter notebook`

4. Navigate in the browser to the jupyter notebook. Open it.

5. Download <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/data/intensity/fm/">  intensity images </a>.

6. In the notebook, update the cell containing the image data localisation.

You can also try to use the segmentation pipeline on the different [Datasets](dataset.md) used.
  
#### Results

You can find sample segmentation results and ground truth at this <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  link </a>. 

