## Training and evaluation datasets 

The datasets used for training and testing the deep learning and non-deep learning pipeline are provided here. Both the training and test dataset have two kinds of files 1) raw confocal images 2) expert annotated ground truth segmentations for each image. All images (raw and ground truth) are in .tif format.

## Training datasets

The training dataset consists of 124 3D confocal image stacks and their corresponding ground truth segmentations, obtained from Arabidopsis thaliana shoot apical meristems (SAM). 

<div align="center">
<img src="../_static/sam.png" alt="Samples from shoot apical meristem" width="400" height="400"/>

</div>

## Evaluation datasets

The evaluation dataset comprises of 10 confocal images of floral meristems from Arabidopsis Thaliana. 6 of the images are from one meristem (FM01), and 4 are from another (FM02). 

<div align="center">
<img src="../_static/fm1.png" alt="Samples from floral meristem 1" width="400" height="300"/>

</div>

<div align="center">
<img src="../_static/fm2.png" alt="Samples from floral meristem 2" width="400" height="300"/>

</div>

## Links to datasets

The raw image and ground truth files may be found in the links below. Results from the 5 segmentation pipelines are available in the 3rd link. Additional 3D image datasets of Ascidian Phalusia mamaliata (PM) embryo and other plant images are in the 4th and 5th links respectively.

1. Training images and ground truth (Shoot Apical Meristem or SAM): <a href=" https://www.repository.cam.ac.uk/handle/1810/262530 ">  https://www.repository.cam.ac.uk/handle/1810/262530  </a> 

2. Test images and ground (Floral Meristem or FM):<a href=" https://www.repository.cam.ac.uk/handle/1810/318119">  https://www.repository.cam.ac.uk/handle/1810/318119 </a> 

3. Sample results from 5 segmentation pipelines with ground truth: <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  Link to segmented and ground truth data </a> 

4. Ascidian Phalusia mamaliata (PM) embryo images: <a href="https://figshare.com/projects/Phallusia_mammillata_embryonic_development/64301">  Phalusia mamaliata (PM) embryo 3D image datasets by (Guignard et al., 2020)* </a> 

5. Plant image dataset: <a href="https://osf.io/uzq3w/">  Assorted plant 3D image datasets by (Wolny et al., 2020)** </a> 


*Guignard, L., Fiúza, U.-M., Leggio, B., Laussu, J., Faure, E., Michelin, G., Biasuz, K., et al. (2020). Contact area-dependent cell communication and the morphological invariance of ascidian embryogenesis. Science, 369(6500)

**Wolny, A., Cerrone, L., Vijayan, A., Tofanelli, R., Barro, A. V., Louveaux, M., Wenzl, C., et al. (2020). Accurate and versatile 3D segmentation of plant tissues at cellular resolution. eLife, 9.
