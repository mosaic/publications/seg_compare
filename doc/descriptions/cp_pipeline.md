## Cellpose
  * <b> Original repository </b>: <a href="https://github.com/MouseLand/cellpose"> https://github.com/MouseLand/cellpose </a> 
  * <b> License </b> : BSD 3-Clause 
  * <b> Paper citation </b> : <i>Stringer C, Wang T, Michaelos M, Pachitariu M. Cellpose: a generalist algorithm for cellular segmentation. Nat Methods. 2021;18: 100–106. doi:10.1038/s41592-020-01018-x </i>
  * <b>Concept</b>: An end-to-end deep learning pipeline which can be trained with 2D images but can perform 3D instance segmentation. It uses a residual 2D-UNet architecture along with image gradient estimations for instance segmentation.

<div align="center">
<img src="../_static/cp_pipeline.png" alt="Cellpose pipeline" width="1000" height="250"/>
<figcaption>Schematic diagram of the Cellpose pipeline</figcaption>
</div>

#### Installation

1. Open a terminal and clone the cellpose repository at <a href="https://github.com/MouseLand/cellpose">  https://github.com/MouseLand/cellpose </a> using:

    `git clone https://github.com/MouseLand/cellpose.git`

2. Navigate to the `cellpose` folder and download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/tree/master/envs/cellpose_env.yaml">  cellpose_env.yaml </a> file (<b>envs/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>) inside the folder.

3. Make sure you are in the `cellpose` folder and create a new environment:

    `conda env create -f cellpose_env.yaml`

4. Activate the environment:
    
    `conda activate cellpose`

5. Install the GPU version of MKL library:

    `pip install mxnet-cu101==version`
    
Note: Check the MKL version required for your computer at  <a href="https://mxnet.apache.org/versions/1.7.0/get_started?">  https://mxnet.apache.org/versions/1.7.0/get_started? </a> 

#### Uses

The segmentation of an image using Cellpose method requires a trained model. This model can be obtained in two different ways:

* Download one of the proposed <a href="https://doi.org/10.6084/m9.figshare.14433590.v1">  trained models </a>
* Train your own model using either your data or the proposed <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> training data  </a> (see [Datasets](dataset.md)).

<b>Train your own model</b>

  * <b>Dataset preparation</b>: The training data can be prepared using 2D image and corresponding ground truth slices from the training dataset. The training filenames are formatted as 

              `wells_000.tif  wells_000_masks.tif`
   

  * <b>Training environment</b>: Invoke the training environment using :

        `conda activate cellpose`. 

  * <b>Model training</b>: Training can be invoked by the following command:

     `python -m cellpose --train --dir  <PATH TO TRAINING DATA FOLDER> --pretrained_model None --use_gpu --n_epochs 1000`

<b>Use a model</b>

  * <b>Post-processing</b>: None. Prediction of trained model on a test image can be invoked within the environment by:

          `python -m cellpose --dir <PATH TO TEST DATA FOLDER>  --pretrained_model <PATH TO TRAINED MODEL FILE>  --save_tif --do_3D --use_gpu --no_npy --diameter 50.0`

#### Results

You can find sample segmentation results and ground truth at this <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  link </a>. 

 
