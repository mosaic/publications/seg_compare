## MASK RCNN+ Seeded watershed
  * <b>Original repository</b>:   <a href="https://github.com/matterport/Mask_RCNN">  https://github.com/matterport/Mask_RCNN </a> 
  * <b>License </b> : MIT License 
  * <b>Paper citation </b> : <i>He K, Gkioxari G, Dollar P, Girshick R. Mask R-CNN. IEEE International Conference on Computer Vision (ICCV). IEEE; 2017. pp. 2980–2988. doi:10.1109/ICCV.2017.322 </i>
  * <b>Concept</b>:  It uses a Mask-R CNN architecture combined with watershed based post processing. The MaskRCNN predicts ROIs in 2D test images (Z slices of confocal stacks)which are used to produce a 3D binary seed image for final watershed based post-processing.

<div align="center">
<img src="../_static/mrcnn_pipeline.png" alt="Unet_ws pipeline" width="1000" height="250"/>
<figcaption>Schematic diagram of the MaskRCNN+Watershed pipeline</figcaption>
</div>

#### Installation

1. Open a terminal and clone the MaskRCNN repository at <a href="https://github.com/matterport/Mask_RC">  https://github.com/matterport/Mask_RC </a> using:

    `git clone https://github.com/matterport/Mask_RCNN.git`

2. Navigate to the Mask_RCNN folder and download the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/tree/master/envs/mrcnn_env.yaml">  mrcnn_env.yaml </a> file (<b>envs/</b> in the <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare">  GitLab</a>) inside the folder.

3. Make sure you are in the Mask_RCNN folder and create a new environment:

    `conda env create -f mrcnn_env.yaml`
    
#### Uses

The segmentation of an image using Cellpose method requires a trained model. This model can be obtained in two different ways:

* Download one of the proposed <a href="https://doi.org/10.6084/m9.figshare.14433590.v1">  trained models </a>
* Train your own model using either your data or the proposed <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> training data  </a> (see [Datasets](dataset.md)).

<b>Train your own model</b>

  * <b>Data preparation</b>: MRCNN takes in 2D images and instance masks for each object to be segmented within an image. For this image Z slices from the training dataset of confocal images are used and instance masks are created from the corresponding ground truth images. An example training dataset is provided below.

  * <b>Training environment</b>: Follow instructions under "Installation" for installing the MaskRCNN training environment. Invoke the MRCNN training environment using :

                     `conda activate mrcnn_watershed`. 

  * <b>Model training</b>: The MaskRCNN training can done by running the notebook <b>MRCNN_training.ipynb</b>


<b>Use a model</b>

  * <b>Post-processing</b>: The trained model can be used for predictions using the inference section of the <b>MRCNN_ training.ipynb</b>. The output of the prediction is a CSV file containing RLE coded regions detected by the MRCNN. For getting the seed image , use this csv file and the notebook <b>MRCNN_create_seed.ipynb</b>. For final 3D segmented output, use the <b>MRCNN_watershed_postprocessing.ipynb</b>. Links to notebooks are in the Downloads folder.

#### Results

You can find sample segmentation results and ground truth at this <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  link </a>. 
