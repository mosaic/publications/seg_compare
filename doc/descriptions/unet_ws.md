## 3D UNET+ Seeded watershed
  * <b>Original repository</b>:  <a href="https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019/"> https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019 </a> 
  * <b>Paper citation </b> : <i>Eschweiler D, Spina TV, Choudhury RC, Meyerowitz E, Cunha A, Stegmaier J. CNN-Based Preprocessing to Optimize Watershed-Based Cell Segmentation in 3D Confocal Microscopy Images. 2019 IEEE 16th International Symposium on Biomedical Imaging (ISBI 2019). IEEE; 2019. pp. 223–227. doi:10.1109/ISBI.2019.8759242 </i>
  * <b>Concept</b>: Use a 3D U-Net to predict cell boundary, cell interior and image background regions, use outputs of the 3D U-Net to generate a seed map for final seeded watershed based 3D segmentation.

<div align="center">
<img src="../_static/unet_ws_pipeline.png" alt="Unet_ws pipeline" width="1000" height="250"/>
<figcaption>Schematic diagram of the 3D UNet+Watershed pipeline</figcaption>
</div>

#### Installation

1. Create and activate a new environment as follows:

    `conda create -n unet_ws python=3.6 anaconda`
    
    `conda activate unet_ws`

2. Install pip:

    `conda install -c anaconda pip`
    
3. Clone the ISBI2019 branch at <a href="https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019/ ">  https://bitbucket.org/jstegmaier/cellsegmentation/src/ISBI2019/  </a> using:

    `git clone -b ISBI2019 https://bitbucket.org/jstegmaier/cellsegmentation.git`

4. Navigate inside the folder `cellsegmentation` and install the dependencies using:
    
    `pip install -r requirements.txt`

    The current (2020) versions of the libraries are the following:

    absl-py==0.9.0
    astor==0.7.1
    certifi==2018.10.15
    cloudpickle==0.6.1
    cycler==0.10.0
    dask==0.19.4
    decorator==4.3.0
    emoji==0.5.1
    gast==0.2.2
    grpcio==1.15.0
    h5py==2.8.0
    Keras==2.2.4
    Keras-Applications==1.0.8
    Keras-Preprocessing==1.0.5
    kiwisolver==1.0.1
    Markdown==3.0.1
    matplotlib==3.0.0
    networkx==2.2
    numpy==1.16.0
    pandas==0.23.4
    Pillow==5.3.0
    protobuf==3.6.1
    pyparsing==2.2.2
    python-dateutil==2.7.3
    pytz==2018.5
    PyWavelets==1.0.1
    PyYAML==3.13
    scikit-image==0.15
    scikit-learn==0.20.0
    scipy==1.1.0
    setuptools==41.0.0
    six==1.11.0
    tensorboard==1.15.0
    tensorflow-gpu==1.15
    termcolor==1.1.0
    toolz==0.9.0
    Werkzeug==0.15

5. Deactivate the conda environment:

    `conda deactivate`

6. Install the `mars` environment following the installation steps described in the [MARS](mars_pipeline.md) pipeline.

#### Uses

The segmentation of an image using `unet_ws` method requires a trained model. This model can be obtained in two different ways:

* Download one of the proposed <a href="https://doi.org/10.6084/m9.figshare.14433590.v1">  trained models </a>
* Train your own model using either your data or the proposed <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> training data  </a> (see [Datasets](dataset.md)).

<b>Train your own model</b>

  * <b>Dataset preparation</b>: For training, save the pathnames of images (membraneX_image) and corresponding ground truth segmentations (masks) in the following format in a text file (filelist.txt): 

                            `path/to/membrane01_image.tif    path/to/membrane01_mask.tif`

                            `path/to/membrane02_image.tif    path/to/membrane02_mask.tif`

  * <b>Training environment</b>: Invoke the training environment using :

        `conda activate unet_ws`. 

 * <b>Model training</b>: The training can be invoked using the following command:

    `python pipeline.py --list_path path_to_filelist.txt --data_path path/to/data --eval_path path/to/save/results/to  --comment experiment_name` 

    After training the prediction on test images can be invoked by :

     `python pipeline.py --no_train --list_path path_to_filelist --data_path path/to/data --comment test_folder` 

       Where test_folder is a folder containing the trained model.hdf5 . The names of the files to test should be mentioned in the filelist in the format shown above. 

<b>Use a model</b>

  * <b>Post-processing</b>: Use predictions from the 3D Unet with the <b>"Unet_WS_postproc.ipynb"</b> notebook to get final segmentation.

#### Results

You can find sample segmentation results and ground truth at this <a href="https://doi.org/10.6084/m9.figshare.14447079.v1">  link </a>. 
