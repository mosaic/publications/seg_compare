## Segmentation pipelines

The steps of a generic deep learning based segmentation pipeline is shown below.

<div align="center">
<img src="../_static/seg_pipeline.png" alt=general pipeline" width="1000" height="250"/>
<figcaption>Schematic diagram a generic deep learning based segmentation pipeline</figcaption>
</div>

Non-deep learning segmentation pipelines do not need the training step. 
Detailed information is provided below for running each of the pipelines either for retraining or using the models directly for segmentation. The training datasets and Python environments for running each of the pipelines are also provided.

## List of pipelines
* [MARS](descriptions/mars_pipeline)
* [PlantSeg](descriptions/plantseg)
* [UNet + Watershed](descriptions/unet_ws)
* [CellPose](descriptions/cp_pipeline)
* [Mask RCNN + Watershed](descriptions/mrcnn)
