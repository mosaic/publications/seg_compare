# 3D visualization of segmentation quality

 * For visualizing quality of outputs from different  3D segmentation pipelines, the browser based visualization platform named Morphonet ( <a href="https://morphonet.org ">  https://morphonet.org </a> ) is used. 

First, the Jaccard index values are computed for a segmented stack with respect to its ground truth. 
Then the 3D segmented stack (in .tif format) is converted to a 3D mesh using Marching Cubes algorithm and the mesh is uploaded to the  Morphonet platform along with the  Jaccard index values. Jaccard indices for each cell can then be viewed as colormaps in a 3D interactive visualization on Morphonet. 

Benefits: Users can 
* check the Jaccard Index value for each cell by clicking on it 
* impose a color mapping of the Jaccard index values on 3D mesh of an image
* locate cell regions where poor Jaccard values are concentrated 
* interact with the segmented dataset in 3D, e.g rotate 360 degrees/zoom/slice through the segmented data in XYZ directions 
* inspect segmentation quality in any inner cell layers 

<div align="center">
<img src="../_static/mn_disp.png" alt="3D Visualization" width="700" height="300"/>

</div>

 
 * The steps for this 3D visualization are below. You will need to:
    * Have a segmented stack and the corresponding ground truth stack
    * Create and/or start the mars environment using the following :

      `conda env create -f mars_env.yaml`

      `conda activate mars`


    * Use the two stacks and run the Volume averaged Jaccard index evaluation.ipynb in this environment. Save the outputs of the ipynb by running the last cell (saves the JI results in a .csv file)
    * Use the 3D_visualization.ipynb to create and upload the information to Morphonet. Give your dataset a name.
    * Open https://morphonet.org and login with your username and password (you can also use  <b>username</b>: "guest",<b>password</b>: "guest2021"). You can now view your dataset with the uploaded mesh. Under the infos tab you can view the Jaccard Index information you uploaded.
    * Go to infos tab, click the name of info you want to display (down arrow), then the white tab that shows up. Clicking on this , you will be able to set a colormap (e.g. "jet"). After setting the colormap click the green tick button and the info will be displayed as color property on the mesh.


## Demo videos

Sample videos showing how segmentation accuracy information may be viewed in 3D on Morphonet. This includes superposing the information on a 3D mesh which has been uploaded in a specific Dataset as described above. Also examples of how users may interact with the uploaded data is shown.

<b>Sample videos:</b>:<a href="https://figshare.com/articles/media/Videos/14686872"> 3D visualization of segmentation accuracy on Morphonet  </a> 


