## Concept
SegCompare is a repository of resources to train, test and evaluate multiple deep learning or non-deep learning algorithms for instance segmentation of 3D images and compare their segmentation quality both in a quantitative and visual manner. 

<div align="center">
<img src="_static/main_pic.png" alt=general pipeline" width="600" height="400"/>
<figcaption> Concept schematic </figcaption>
</div>

The purpose of the resources is to enable anyone to use some highly efficient 3D segmentation pipelines for their own data, with or without retraining the models. The utilities of this repository are the following:
* For users wanting to segment their data: They can directly use one of the trained models and pipelines described here to segment their data.*
* For users designing a new 3D segmentation pipeline: They can use the fully annotated image datasets and evaluation methods to estimate the quality of their method.
* For users having segmented data and expert ground truth: They can use this repository to evaluate the quality of their segmentations with quantititative metrics and 3D visualizations.

## Gitlab repository

<a href="https://gitlab.inria.fr/mosaic/publications/seg_compare"> https://gitlab.inria.fr/mosaic/publications/seg_compare </a> 

## Publication

Our paper describing the background research on the quantitative comparison of different segmentation pipelines may be found here:<a href="https://www.biorxiv.org/content/10.1101/2021.06.09.447748v1">  Assessment of deep learning algorithms for 3D instance segmentation of confocal image datasets </a> 

## Credits

Anuradha Kar(1), Yassin Refahi(2), Manuel Petit(1), Guillaume Cerutti(1), Jonathan Legrand (1), Christophe Godin(1), Jan Traas(1)

Affiliations:

(1): Laboratoire RDP, Université  de Lyon 1, ENS-Lyon INRAE , INRIA,  CNRS, UCBL, 69364 Lyon, France

(2): Université de Reims Champagne Ardenne, INRAE, FARE, UMR A 614, 51097 Reims, France

