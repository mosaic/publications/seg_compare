## Downloads
Here you will find links to all the notebooks and datasets used for training, testing and evaluating five segmentation pipelines. Link to video demos showing the 3D segmentation quality visualization process is also included.

## Notebooks

<b>Running the Mars segmentation pipeline:</b> </b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/MARS_segmentation.ipynb
"> MARS_segmentation.ipynb  </a> 

<b>Post-processing 3 class outputs from 3D Unet (Unet+WS pipeline):</b><a href=" https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/Unet_Ws_postprocessing.ipynb"> Unet_Ws_postprocessing.ipynb  </a> 

<b> Mask RCNN training: </b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/MRCNN_training.ipynb"> MRCNN_training.ipynb  </a> 

<b> Mask RCNN seed creation: </b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/MRCNN_create_seed.ipynb"> MRCNN_create_seed.ipynb  </a> 

<b> Mask RCNN 3D watershed postprocesssing: </b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/MRCNN_watershed_postprocessing.ipynb"> MRCNN_watershed_postprocessing  </a> 

<b>Compute volume averaged Jaccard Index (using a segmented stack and ground truth stack):</b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/Volume averaged Jaccard index evaluation.ipynb"> Volume averaged Jaccard index evaluation.ipynb  </a> 



<b>Estimate segmentation quality (rates of over and under-segmentation):</b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/ Characterization of the segmentation errors (underseg, overseg,...).ipynb">  Characterization of the segmentation errors (underseg, overseg,...).ipynb  </a> 

<b>3D Visualization of segmentation quality on Morphonet :</b> <a href="https://gitlab.inria.fr/mosaic/publications/seg_compare/-/blob/master/doc/notebooks/3D_visualization.ipynb"> 3D_visualization.ipynb  </a> 

## Data

<b>Training images:</b> <a href="https://www.repository.cam.ac.uk/handle/1810/262530 "> https://www.repository.cam.ac.uk/handle/1810/262530  </a> 

<b>Test images:</b> <a href=" https://www.repository.cam.ac.uk/handle/1810/318119">  https://www.repository.cam.ac.uk/handle/1810/318119 </a> 

<b>SegCompare data and model repository:</b> <a href="https://figshare.com/projects/3D_segmentation_and_evaluation/101120 "> 3D segmentation and evaluation </a> 

<b>Sample meshes for 3D visualization on Morphonet:</b>:<a href="https://figshare.com/articles/dataset/Meshes_for_Morphonet/14447097 "> Meshes for Morphonet  </a> 

<b>Sample accuracy results for 3D visualization on Morphonet:</b>:<a href="https://figshare.com/articles/dataset/Accuracy_results/14447181"> Accuracy results to visualize on Morphonet  </a> 

## Videos

<b>3D visualization example:</b>: <a href="https://figshare.com/articles/media/Videos/14686872"> 3D visualization of segmentation accuracy on Morphonet </a> 

