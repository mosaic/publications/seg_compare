# SegCompare

This repository contains the coding implementations and installation instructions for the segmentation pipelines and segmentation evaluation methods which are used in the article "Benchmarking of deep learning algorithms for 3D instance segmentation of confocal image datasets".
Authors: Anuradha Kar, Manuel Petit, Yassin Refahi, Guillaume Cerutti, Christophe Godin, Jan Traas.  2021.

#### Documentation page: 
https://mosaic.gitlabpages.inria.fr/publications/seg_compare/

## Pre-requisites

To run the codes or pipelines in this repository,  it is necessary to install Python packages. 
Please install CONDA on your system:
(https://conda.io/projects/conda/en/latest/user-guide/install)

## Segmentation pipelines

### MARS
- Details and installation: https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/mars_pipeline.html

### Plantseg
- Details and installation: https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/plantseg.html

### 3D UNet with seeded watershed post processing (UNet_WS)
- Details and installation: https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/unet_ws.html

### Cellpose
- Details and installation: https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/cp_pipeline.html

### MaskRCNN with seeded watershed post processing (MRCNN_WS)
- Details and installation: 
https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/mrcnn.html

## Datasets

- Description and links : https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/dataset.html

## Segmentation evaluation methods
- Details and installations: 
https://mosaic.gitlabpages.inria.fr/publications/seg_compare/evaluation.html

## 3D visualization of segmentation accuracy
- Details and installations: 
https://mosaic.gitlabpages.inria.fr/publications/seg_compare/descriptions/morphonet.html

## Jupyter notebooks, sample data, videos
- https://mosaic.gitlabpages.inria.fr/publications/seg_compare/citation.html 

## License

INRIA